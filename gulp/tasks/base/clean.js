var gulp = require('gulp')
var clean = require('gulp-clean')
var config = require('../../config').config;

// "Clean contents of ./dist
gulp.task('clean', function() {
    return gulp.src(config.paths.dest.base, {read: false})
        .pipe(clean({force: true}))
})
