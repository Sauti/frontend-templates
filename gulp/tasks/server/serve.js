var gulp = require('gulp')
var gutil = require('gulp-util')
var browserSync = require('browser-sync')
var config = require('../../config').config;


gulp.task('serve', function() {
    return browserSync({
        server: {
            baseDir: config.paths.dest.base
        },
        port: config.HTTP_SERVER_PORT,
        open: config.HTTP_SERVER_OPEN_IN_BROWSER,
        files: config.paths.dest.livereload
    });
});
