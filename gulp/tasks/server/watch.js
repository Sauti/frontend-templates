var gulp = require('gulp');
var config = require('../../config').config;


gulp.task('watch-stylesheets', function() {
    gulp.watch(config.paths.src.stylesheets, ['stylesheets']);
});

gulp.task('watch-templates', function() {
    gulp.watch(config.paths.src.templates, ['templates']);
});

gulp.task('watch', ['watch-stylesheets', 'watch-templates']);
