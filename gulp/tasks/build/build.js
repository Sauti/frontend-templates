var gulp = require('gulp');
var config = require('../../config');
var runSequence = require('run-sequence');


gulp.task('build', function(done) {
    runSequence(
        ['clean'],
        ['stylesheets'],
        ['templates'],
        done
    );
});
