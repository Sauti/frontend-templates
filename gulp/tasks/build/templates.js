var gulp = require('gulp');
var config = require('../../config').config;


gulp.task('templates', function () {
    return gulp.src(config.paths.src.templates)
        .pipe(gulp.dest(config.paths.dest.base, { base: './'}))
});
