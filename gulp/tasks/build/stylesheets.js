var gulp = require('gulp');
var gutil = require('gulp-util')
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var notify = require('gulp-notify');
var plumber = require('gulp-plumber');
var minifyCSS = require('gulp-minify-css')
var gulpIf = require('gulp-if');
var config = require('../../config').config;
var gzip = require('gulp-gzip');


gulp.task('stylesheets', function () {
    return gulp.src(config.paths.src.stylesheets)
        .pipe(plumber(function (error) {
            gutil.log(gutil.colors.red(error.message))
            return this.emit('end');
        }))
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(gulpIf(config.MINIFY_ASSETS, minifyCSS({processImport: false})))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(config.paths.dest.stylesheets))
        .pipe(gzip({threshold: '1kb'}))
        .pipe(gulp.dest(config.paths.dest.stylesheets))
        .on('error', notify.onError(function (error) {
            return error.message
        }));
});
