var process = require('process');
var gulp = require('gulp');


exports.config = {
    
    HTTP_SERVER_PORT: 3001,
    HTTP_SERVER_OPEN_IN_BROWSER: true,

    MINIFY_ASSETS: false,

    paths: {
        
        src: {
            
            stylesheets: [
                "src/css/**/*.scss"
            ],
            templates: [
                "src/**/*.html"
            ]
            
        },
        
        dest: {
            
            base: "run",
            
            stylesheets: "run/css",

            livereload: [
                "run/**/*",
                "!run/**/*.map"
            ]
            
        }
        
    }
    
};
