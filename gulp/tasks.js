require("./tasks/base/clean");

require("./tasks/build/stylesheets");
require("./tasks/build/templates");
require("./tasks/build/build");

require("./tasks/server/watch");
require("./tasks/server/serve");

require("./tasks/default");
